public class Board {
	//fields
	private Square[][] tictactoeBoard;
	
	//constructor
	public Board() {
		this.tictactoeBoard = new Square[3][3];
		for(int i=0; i<this.tictactoeBoard.length; i++) {
			for(int j=0; j<this.tictactoeBoard[i].length; j++) {
				this.tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	//toString
	public String toString() {
		String board = "  0 1 2";
		for(int row = 0; row < tictactoeBoard.length; row++) {
			board += "\n" + row + " ";
			for(int col = 0; col < tictactoeBoard[row].length; col++) {
				board += this.tictactoeBoard[row][col] + " ";
			}
		}
		return board;
	}
	
	//instance methods
	public boolean placeToken(int row, int col, Square playerToken) {
		if(row < 0 || row >= 3 || col < 0 || col >= 3) {
			return false;
		}
		
		if(this.tictactoeBoard[row][col] == Square.BLANK) {
			this.tictactoeBoard[row][col] = playerToken;
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean checkIfFull() {
		for(Square[] you : this.tictactoeBoard) {
			for(int i = 0; i < this.tictactoeBoard.length; i++) {
				if(you[i] == Square.BLANK) {
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkifWinningHorizontal(Square playerToken) {
		int winning = 0;
		for(int row = 0; row < this.tictactoeBoard.length; row++, winning = 0) {
			for(int col=0; col < this.tictactoeBoard[row].length; col++) {
				if(this.tictactoeBoard[row][col] == playerToken) {
					winning += 1;
				}
				if(winning == 3) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean checkifWinningVertical(Square playerToken) {
		int winning = 0;
		for(int col = 0; col < this.tictactoeBoard.length; col++, winning = 0) {
			for(int row=0; row < this.tictactoeBoard[col].length; row++) {
				if(this.tictactoeBoard[row][col] == playerToken) {
					winning += 1;
				}
				if(winning == 3) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean checkIfWinningDiagonal(Square playerToken) {
		int winning = 0;
		for(int diag = 0; diag < this.tictactoeBoard.length; diag++) {
			if(this.tictactoeBoard[diag][diag] == playerToken) {
				winning += 1;
			}

			if(winning == 3) {
				return true;
			}
		}
		
		winning = 0;

		for(int rDiag = this.tictactoeBoard.length - 1, lDiag = 0; rDiag >= 0; rDiag--, lDiag++) {
			if(this.tictactoeBoard[lDiag][rDiag] == playerToken) {
				winning += 1;
			}

			if(winning == 3) {
				return true;
			}
		}
		return false;

	}

	public boolean checkIfWinning(Square playerToken) {
		if(checkifWinningHorizontal(playerToken) || checkifWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken)) {
			return true;
		}
		else {
			return false;
		}
	}
}