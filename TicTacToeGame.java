import java.util.Scanner;
public class TicTacToeGame {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		boolean playing = true;
		char playAgain = 'y';
		while(playing) {
			playGame();
			System.out.println("\n" + "Would you like to play again? (y or n)");
			playAgain = reader.next().charAt(0);
			if(playAgain == 'y') {
			}
			else {
				playing = false;
				System.out.println("\n" + "Thank you for playing!");
			}
		}
	}

	public static void playGame() {
		//Variables
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		int row = 0;
		int col = 0;
		Scanner reader = new Scanner(System.in);
		
		//running the game
		while(!gameOver) {
			System.out.println(board);
			if(player == 1) {
				playerToken = Square.X;
			}
			else {
				playerToken = Square.O;
			}
			
			System.out.println("Player " + player + ": Where would you like to place your token?(Row, Col)");
			row = reader.nextInt();
			col = reader.nextInt();
			
			while(!board.placeToken(row, col, playerToken)) {
				System.out.println("You entered either a wrong value or a square that already has a token place. Please re-enter: (Row, Col)");
				row = reader.nextInt();
				col = reader.nextInt();
			}

			if(board.checkIfWinning(playerToken)) {
				System.out.println(board);
				System.out.println("Congratulations player " + player + ": you are the winner!");
				gameOver = true;
			}
			else if(board.checkIfFull()) {
				System.out.println(board);
				System.out.println("It's a tie!");
				gameOver = true;
			}
			else {
				if(player == 1) {
					player += 1;
				}
				else {
					player -= 1;
				}
			}
		}
	}
}